<?php

namespace model;

use PDO;
use service\Connection;

class Biglietto
{
    public function getBigliettoByCode($code) {
        /** @var $pdo PDO */
        $pdo = Connection::getConnection();
        $statment = $pdo->prepare("SELECT * FROM biglietto where code='{$code}'");
        $statment->execute();
        $result = $statment->setFetchMode(PDO::FETCH_ASSOC);
        if (!$result)
            throw new \Exception("error");
        return $statment->fetchAll();
    }
}
