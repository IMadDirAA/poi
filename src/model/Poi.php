<?php

namespace model;

use PDO;
use service\Connection;

class Poi
{
    public function getPoiByLocation($location) {
        /** @var $pdo PDO */
        $pdo = Connection::getConnection();
        $statment = $pdo->prepare("SELECT * FROM Poi");
        $statment->execute();
        $result = $statment->setFetchMode(PDO::FETCH_ASSOC);

        if (!$result)
            throw new \Exception("error");
        $result = $statment->fetchAll();

        $accessablePois = [];
        foreach ($result as $poi) {
            $gps = $poi['GPS'];
            $gps = explode(',', $gps);
            // checl gps to location
            // if true add to accessablePois
            $accessablePois[] = $poi;
        }
        return $accessablePois;
    }
}
