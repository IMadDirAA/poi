<?php

namespace Controller;

use model\Biglietto;
use service\Template;

class UserController
{
    public function login() {
        if (isset($_SESSION["data"])) {
            header("Location: poi");
        }
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $code = $_POST['code'];
            $bigliettoModel = new Biglietto();
            $data = $bigliettoModel->getBigliettoByCode($code);
            if (count($data) == 1) {
                session_start();
                $_SESSION["data"]=$data;
                header("Location: poi");
            } else {
                Template::render('login', ["error" => "incorrect code"]);
            }
        } else {
            Template::render('login');
        }
    }
}
