<?php

namespace Controller;

use model\Poi;
use service\Template;

class PoiController
{
    public function index() {
        $poiModel = new Poi();
        $data = ["pois" => $poiModel->getPoiByLocation('')];
        Template::render('poi', $data);
    }
}