<?php

namespace Controller;

use service\Template;

class DefaultController
{
    public function index() {
        header('Location: login');
    }
}