<?php
use model\Questionario;
$data = $GLOBALS["TEMPLATE_DATA"];
//echo "<pre>";
//var_dump($data);
?>
<head>
    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style>
        body{background: #000}.card{border: none;height: 320px}.forms-inputs{position: relative}.forms-inputs span{position: absolute;top:-18px;left: 10px;background-color: #fff;padding: 5px 10px;font-size: 15px}.forms-inputs input{height: 50px;border: 2px solid #eee}.forms-inputs input:focus{box-shadow: none;outline: none;border: 2px solid #000}.btn{height: 50px}.success-data{display: flex;flex-direction: column}.bxs-badge-check{font-size: 90px}
    </style>
    <script>
        var app = new Vue({
            el: '#form1',
            data: function () {
                return {
                    email : "",
                    emailBlured : false,
                    valid : false,
                    submitted : false,
                    password:"",
                    passwordBlured:false
                }
            },

            methods:{

                validate : function(){
                    this.emailBlured = true;
                    this.passwordBlured = true;
                    if( this.validEmail(this.email) && this.validPassword(this.password)){
                        this.valid = true;
                    }
                },

                validEmail : function(email) {

                    var re = /(.+)@(.+){2,}\.(.+){2,}/;
                    if(re.test(email.toLowerCase())){
                        return true;
                    }
                },

                validPassword : function(password) {
                    if (password.length > 7) {
                        return true;
                    }
                },

                submit : function(){
                    this.validate();
                    if(this.valid){
                        this.submitted = true;
                    }
                }
            }
        });
    </script>
</head>

<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card px-5 py-5" id="form1">
                <form action="login" method="post">
                    <div class="form-data" >
                        <div class="forms-inputs mb-4"> <span>PASSWORD CODE</span> <input autocomplete="off" type="text" name="code">
                            <?php if (isset($data["error"])) { ?><div style="color:red">invalid code</div><?php } ?>
                        </div>
                        <div class="mb-3"> <button type="submit" class="btn btn-dark w-100">Login</button> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

